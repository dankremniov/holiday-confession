require("owl.carousel");

(function() {
	function init()  {
		$(document).ready(function(){
			$('.owl-carousel').owlCarousel({
				responsive: {
					0: { items: 1 },
					500: { items: 2 },
					800: { items: 3 }

			    },
				nav: true,
				navText: ["<span>Previous</span>","<span>Next</span>"],
				dots: false,
				loop: true,
				animateOut: true,
				animateIn: true,
				responsiveRefreshRate: 10
			});
			$('.js-close').click(function() {
				$('.js-modal').fadeOut();
			});
			$('.js-open').click(function() {
				$('.js-modal').fadeIn();
			});
			$('.js-main-fb-share').click(mainToFacebook);
			$('.js-inner-fb-share').click(innerToFacebook);
			$('.js-main-twitter-share').click(mainToTwitter);
			$('.js-inner-twitter-share').click(innerToTwitter);
		});
	}
	function mainToFacebook() {
		FB.ui({
			method: 'feed',
			link: window.location.href,
			name: "Holiday Confessions",
		});
	}
	function innerToFacebook(e) {
		FB.ui({
			method: 'feed',
			link: window.location.href + $(e.currentTarget).parent().data('story') + '.html',
		});
	}
	function mainToTwitter(e) {
		var element = $(e.currentTarget);
		var params = {
			text: element.parent().data('tweet')
		};
		
		element.prop('href', 'https://twitter.com/intent/tweet?' + $.param(params));
		var width  = 575,
        	height = 400,
	        left   = ($(window).width()  - width)  / 2,
	        top    = ($(window).height() - height) / 2,
	        url    = this.href,
	        opts   = 	'status=1' +
                 		',width='  + width  +
                 		',height=' + height +
                 		',top='    + top    +
                 		',left='   + left;
    
    	window.open(url, 'twitter-share', opts);
	    return false;
	}
	function innerToTwitter(e) {
		var element = $(e.currentTarget);
		var params = {
			text: element.parent().data('tweet')
		};
		element.prop('href', 'https://twitter.com/intent/tweet?' + $.param(params));
		var width  = 575,
        	height = 400,
	        left   = ($(window).width()  - width)  / 2,
	        top    = ($(window).height() - height) / 2,
	        url    = this.href,
	        opts   = 	'status=1' +
                 		',width='  + width  +
                 		',height=' + height +
                 		',top='    + top    +
                 		',left='   + left;
    
    	window.open(url, 'twitter-share', opts);
	    return false;
	}
	init();
})();
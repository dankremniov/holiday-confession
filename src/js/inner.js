(function() {
	function init()  {
		$('.js-close').click(function() {
			$('.js-modal').fadeOut();
		});
		$('.js-open').click(function() {
			$('.js-modal').fadeIn();
		});
		$('.js-inner-fb-share').click(innerToFacebook);
		$('.js-inner-twitter-share').click(innerToTwitter);
		$('.js-inner-twitter-share').click(innerToTwitter);
	}
	function innerToFacebook() {
		FB.ui({
			method: 'feed',
			link: window.location.href,
		});
	}
	function innerToTwitter(e) {
		e.preventDefault();
		var element = $(e.currentTarget);
		// e.preventDefault();
		var params = {
			text: element.parent().data('tweet')
		};
		
		element.prop('href', 'https://twitter.com/intent/tweet?' + $.param(params));
		var width  = 575,
        	height = 400,
	        left   = ($(window).width()  - width)  / 2,
	        top    = ($(window).height() - height) / 2,
	        url    = this.href,
	        opts   = 	'status=1' +
                 		',width='  + width  +
                 		',height=' + height +
                 		',top='    + top    +
                 		',left='   + left;
    
    	window.open(url, 'twitter-share', opts);
	    return false;
	}
	init();
})();